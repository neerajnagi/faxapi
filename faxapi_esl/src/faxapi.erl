%% @author Mochi Media <dev@mochimedia.com>
%% @copyright 2010 Mochi Media <dev@mochimedia.com>

%% @doc faxapi.

-module(faxapi).
-author("Mochi Media <dev@mochimedia.com>").
-export([start/0, stop/0]).

ensure_started(App) ->
    case application:start(App) of
        ok ->
            ok;
        {error, {already_started, App}} ->
            ok
    end.


%% @spec start() -> ok
%% @doc Start the faxapi server.
start() ->
    faxapi_deps:ensure(),
    ensure_started(crypto),
    application:start(faxapi).


%% @spec stop() -> ok
%% @doc Stop the faxapi server.
stop() ->
    application:stop(faxapi).
