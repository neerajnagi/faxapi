%% @author Mochi Media <dev@mochimedia.com>
%% @copyright 2010 Mochi Media <dev@mochimedia.com>

%% @doc Web server for faxapi.

-module(faxapi_web).
-author("neeraj nagi").

-include("/root/erl_dep/eredis/include/eredis.hrl").
-include("/root/erl_dep/eredis/include/eredis_sub.hrl").
-import(eredis,[start_link/0, start_link/1, start_link/2, start_link/3, start_link/4, start_link/5, stop/1, q/2, q/3, qp/2, qp/3, q_noreply/2 ]).
-import(eredis_sub,[subscribe/2,ack_message/1,controlling_process/1]).
-export([start/1, stop/0,loop/2,convert_and_dispatch/3]).
-import(smscountry,[register_did/2 ,update_email/2,  insert_fax/4, get_did/1]).
-import(proplists,[get_value/2,get_value/3]).
-import(uuid,[generate/0]).
%% External API

start(Options) ->
Freeswitch = list_to_atom(os:getenv("FS_NODE")),
mnesia:start(), 
{DocRoot, Options1} = get_option(docroot, Options),
    Loop = fun (Req) ->
                   ?MODULE:loop(Req, DocRoot)
           end,
    mochiweb_http:start([{name, ?MODULE}, {loop, Loop} | Options1]).

stop() ->
   mnesia:dump_tables([fax,did]),
    mochiweb_http:stop(?MODULE).

loop(Req, DocRoot) ->
    "/" ++ Path = Req:get(path),
    try
        case Req:get(method) of
            Method when Method =:= 'GET'; Method =:= 'HEAD' ->
                case re:run(Path,"repo.*$") of
		    {match,[{St,Ln}|_]} ->
			RepoFilePath = string:substr(Path,St+1,Ln),
		        {match,[{St1,Ln1}|_]}	= re:run(RepoFilePath,"[^\/]+$" ),
			 Req:serve_file(string:substr(RepoFilePath,St1+1,Ln1), "/root/fax_repo");
                    _ ->
                        Req:serve_file(Path, DocRoot)
                end;
            'POST' ->




                case Path of
		"send_fax" -> send_fax(Req,"/root/fax_repo/",[".pdf"]);
		"update_email" -> update_email(Req);
                "did_metadata" ->
                        [{_,Did}|_] = Req:parse_post(),
                        [{did,_,Email  }|_] = smscountry:get_did( Did ),
                         Req:respond({200, [{"Content-Type", "text/json"}],
                      "{\n  \"email\": \""++ Email  ++  "\"\n}"    });
		 "sendfax" -> sendfax(Req);

                  _ ->
                        Req:not_found()
                end;

            _ ->
                Req:respond({501, [], []})
        end
    catch
        Type:What ->
            Report = ["web request failed",
                      {path, Path},
                      {type, Type}, {what, What},
                      {trace, erlang:get_stacktrace()}],
            error_logger:error_report(Report),
            %% NOTE: mustache templates need \ because they are not awesome.
            Req:respond({500, [{"Content-Type", "text/plain"}],
                         "request failed, sorry\n"})
    end.

%% Internal API

get_option(Option, Options) ->
    {proplists:get_value(Option, Options), proplists:delete(Option, Options)}.

%%
%% Tests
%%
-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").

you_should_write_a_test() ->
    ?assertEqual(
       "No, but I will!",
      "Have you written any tests?"),
    ok.

-endif.

send_fax(Req, FaxDir, ValidExtensions) ->
    FileHandler = fun(Filename, ContentType) -> handle_file(Filename, ContentType) end,
    Files = mochiweb_multipart:parse_form(Req, FileHandler),
{OriginalFilename, _, TempFilename} = proplists:get_value("fax", Files),

 Recipient = proplists:get_value("recipient", Files),
io:format("~p   is the recipient", [Recipient]),
    case lists:member(filename:extension(OriginalFilename), ValidExtensions) of
        true ->
            Destination = FaxDir ++ OriginalFilename,
	{A,B,C} = erlang:now(),
	FaxUUID = "/root/fax_repo/Fax_"++ integer_to_list(A) ++ integer_to_list(B)++integer_to_list(C)++".tiff",

            case file:rename(TempFilename, Destination) of
                ok ->
                    Url = "/",
                    Req:respond({302, [{"Location", Url}], "Redirecting to " ++ Url});
                {error, Reason} ->
                    file:delete(TempFilename),
                    html_response(Req, "An error occured whilst trying to move your file: " ++ atom_to_list(Reason) ++ ". Does the destination directory exist?")
            end,

	Pid = spawn(?MODULE,convert_and_dispatch , [FaxUUID,Destination,Recipient]),
    	Pid;
 

        false ->
            file:delete(TempFilename),
            html_response(Req, "Invalid file type " ++ filename:extension(OriginalFilename)   ++  " File extension must be one of: " ++ string:join(ValidExtensions, ", ") ++ ". <a href=\"/\">Try again?</a>")
    end.

html_response(Req, Response) ->
    Req:ok({"text/html", [], Response}).


handle_file(Filename, ContentType) ->
    TempFilename = "/tmp/" ++ atom_to_list(?MODULE) ++ integer_to_list(erlang:phash2(make_ref())),
    {ok, File} = file:open(TempFilename, [raw, write]),
    chunk_handler(Filename, ContentType, TempFilename, File).

chunk_handler(Filename, ContentType, TempFilename, File) ->
    fun(Next) ->
        case Next of
            
            eof ->
                % End of part: close file and return details of the upload
                file:close(File),
                {Filename, ContentType, TempFilename};
                
            Data ->
                % More data to write to the file
                file:write(File, Data),
                chunk_handler(Filename, ContentType, TempFilename, File)
        end
    end.


update_email(Req) ->
DidAndEmail = Req:parse_post(),
Did = proplists:get_value("did",DidAndEmail),
NewEmail = proplists:get_value("email",DidAndEmail),
smscountry:update_email(Did,NewEmail),
  Req:respond({302, [{"Location", "/"}], "Redirecting to Home Page"}).

convert_and_dispatch(FaxUUID_,Destination_,Recipient_)->
Freeswitch =list_to_atom( os:getenv("FS_NODE")),
io:format("starting pdf to tif conversion"),
ConversionResult1 = os:cmd("gs -q -r204x196 -g1728x2156 -dNOPAUSE -dBATCH -dSAFER -sDEVICE=tiffg3 -sOutputFile=" ++ FaxUUID_  ++ " -- " ++ Destination_ ),
io:format("done converting"),
  {sending_fax, Freeswitch} ! {api,originate,"{ignore_early_media=true,absolute_codec_string='PCMU,PCMA',fax_enable_t38=true,fax_verbose=true,fax_use_ecm=true,fax_enable_t38_request=true}sofia/gateway/trunk/"++ Recipient_ ++  " &txfax('"++ FaxUUID_  ++ "')"} , receive Z_ -> Z_ after 1000 -> timeout end.




sendfax(Req) ->
Freeswitch = list_to_atom(os:getenv("FS_NODE")),
SendFaxParams = Req:parse_post(),
To = proplists:get_value("to",SendFaxParams,"missing_field"),
From = proplists:get_value("from",SendFaxParams,"missing_field"),
CompanyName = proplists:get_value("company_name", SendFaxParams,"missing_field"),
FaxNumber = proplists:get_value("fax_number",SendFaxParams,"missing_field"),
Subject = proplists:get_value("subject",SendFaxParams,"missing_field"),
Body = proplists:get_value("body",SendFaxParams,"missing_field"),
Attachments = proplists:get_value("attachments",SendFaxParams,"missing_field"),
Recipient= proplists:get_value("recipient",SendFaxParams,"missing_field"),
Quality=proplists:get_value("quality",SendFaxParams,"missing_field"),
Color=proplists:get_value("color",SendFaxParams,"missing_field"),
Callback = proplists:get_value("callback",SendFaxParams,"missing_field"),
io:format(lists:concat(["to:",To," companyname:",CompanyName," faxnumber:",FaxNumber," subject:",Subject," body:",Body," attachemtns:",Attachments," recipient:",Recipient," quality:",Quality," color:",Color," callback:", Callback])),
FaxUniqueId =  uuid:generate(),
FrontPageTxt= To ++ "\n" ++ CompanyName ++ "\n\n" ++ "Subject: " ++ Subject ++ "\n" ++ Body,
ConversionResult = os:cmd("ruby /root/faxapi/src/attachments_to_tif.rb \"" ++ Attachments ++ "\"  \"" ++ FaxUniqueId ++ "\"  \"" ++ FrontPageTxt ++ "\"  \"" ++ Quality ++ "\" \"" ++ Color ++"\""),
%%io:format("~p -----------------------",[ConversionResult]).


if 
	ConversionResult /= "" -> 
			 Req:respond({200, [{"Content-Type", "text/json"}], lists:concat(["{\"status\":\"failed\",\"reason\":\"",ConversionResult,"\"}"])});	
	true ->
			{ok, P} = eredis:start_link([]),
			eredis:q(P, ["PUBLISH", "ruby_channel", lists:concat(["{\"fax_id\":\"",FaxUniqueId,"\",\"from\":\"",From,"\",\"fax_number\":\"",FaxNumber,"\"}"  ])]),
			eredis_client:stop(P),
			Req:respond({200,[{"Content-Type","text/json"}],"{\"status\":\"dispatched" ++ "\",\"fax_id\":\""++ FaxUniqueId  ++"\"}"})	
			


end.



