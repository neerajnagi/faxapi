attachments = ARGV[0]
uuid = ARGV[1]
REPO_DIR="/root/fax_repo/"
msg = ARGV[2]
quality = ARGV[3] # high or low
color= ARGV[4] # color or grey

if( color.downcase != 'color' &&  color != 'gray')
	color == 'gray'
end

if quality=='low'
	RESOLUTION="-r204x98 -g1728x1078"
	DENSITY="204x98"
	RESIZE="1728x1078"
else
	RESOLUTION="-r204x196 -g1728x2156"
	DENSITY="204x196"
	RESIZE="1728x2156"
end

if attachments.nil?
	puts "attachments missing"
end

if uuid.nil?
	puts "uuid missing"

end

if msg.nil?
	puts "frontpage message missing"
end

if quality.nil?
	puts "quality missing"
end

if color.nil?
	puts "color missing"
end

#puts "attachments->#{attachments} , uuid->#{uuid} , repo_dir->#{REPO_DIR}, msg->#{msg}"

File.open("/tmp/0_#{uuid}.txt", 'w+') {|f| f.write(msg) }


PREF = "/tmp/#{uuid+'_'}"
$arr = []

$arr[0]="/tmp/0_#{uuid}.txt"

system("enscript -q #{$arr[0]} -o #{$arr[0].gsub(/[^\.]+$/,"ps")} > /dev/null ")
system("gs -q #{RESOLUTION} -dNOPAUSE -dBATCH -dSAFER -sDEVICE=tiffg3 -sOutputFile=" + $arr[0].gsub(/[^\.]+$/,'tiff')  + " -- " + $arr[0].gsub(/[^\.]+$/,"ps") +" > /dev/null  ")



attachments.split(',').each do |attachment|
$arr[$arr.size]=PREF+$arr.size.to_s+"_"+attachment.strip.match(/[^\/]+$/)[0]
system("wget -q -O #{$arr[$arr.size-1]} #{attachment.strip} > /dev/null  ") 
if File.size($arr[$arr.size-1]) == 0
	puts "attachment #{attachment.strip} not available for downloading"
	exit
end

case attachment.strip.match(/[^\.]+$/)[0].downcase

when 'pdf' ,  'ps'

if(color == 'gray')
        system("convert  -quiet #{$arr[$arr.size-1]}  -colorspace Gray #{$arr[$arr.size-1]}  > /dev/null ")
end


system("gs -q #{RESOLUTION} -dNOPAUSE -dBATCH -dSAFER -sDEVICE=tiffg4 -sOutputFile=" + $arr[$arr.size-1].gsub(/[^\.]+$/,'tiff')  + " -- " + $arr[$arr.size-1] + " > /dev/null ")





when 'txt' , 'text'
system("enscript -q #{$arr[$arr.size-1]} -o #{$arr[$arr.size-1].gsub(/[^\.]+$/,"ps")}  > /dev/null ")
system("gs -q #{RESOLUTION} -dNOPAUSE -dBATCH -dSAFER -sDEVICE=tiffg3 -sOutputFile=" + $arr[$arr.size-1].gsub(/[^\.]+$/,'tiff')  + " -- " + $arr[$arr.size-1].gsub(/[^\.]+$/,"ps")  +" > /dev/null "  )


when 'jpg' , 'jpeg' , 'png' , 'gif' , 'bmp', 'tiff', 'tif'

if(color == 'gray')
        system("convert  -quiet #{$arr[$arr.size-1]}  -colorspace Gray #{$arr[$arr.size-1]}  > /dev/null ")
end

system("convert -quiet  #{$arr[$arr.size-1]}  -compress Group4    #{$arr[$arr.size-1].gsub(/[^\.]+$/,'tiff')} > /dev/null ")

else
puts "unsupported format for attachment(#{$arr[$arr.size-1]})"
exit
end
end
tiff_list = ''

$arr.each do |tiff|
tiff_proper = tiff.gsub(/[^\.]+$/,'tiff')
tiff_list += tiff_proper+' '
end

system("tiffcp #{tiff_list} #{REPO_DIR+uuid}.tiff > /dev/null " )
system("rm -f #{uuid}* > /dev/null ")
