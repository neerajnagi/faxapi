-module(smscountry). 
-export([
init/0,
register_did/2,
update_email/2,
insert_fax/4,
get_did/1

]). 

-include_lib("stdlib/include/qlc.hrl").
-include("smscountry.hrl").

%%----------------------------------------

init()                             ->
    mnesia:create_table(did,
                        [{attributes, record_info(fields, did)}]),
    mnesia:create_table(fax,
                        [{attributes, record_info(fields, fax)}]).

%%----------------------------------------

register_did(Did, Email) ->
   
Did_new = #did{did=Did,email=Email},
F = fun() ->
mnesia:write(Did_new)
end,
mnesia:transaction(F).

%%----------------------------------------

update_email(Did,Email) ->

    F = fun() ->
                [E] = mnesia:read({did, Did}),
                New = E#did{email = Email},
                mnesia:write(New)
        end,
    mnesia:transaction(F).



%%----------------------------------------------



insert_fax(From,To,Direction,Location) ->
   
Fax_new = #fax{to=To, from=From, direction=Direction, location=Location},
F = fun() ->
mnesia:write(Fax_new)
end,
mnesia:transaction(F).

get_did(Did) ->
F = fun() ->
mnesia:read({did,Did})
end,
{atomic,Data} = mnesia:transaction(F),
Data.

