require "ruby_fs"
require "redis"
require "json"
require 'mechanize'
require 'logger'
logger = Logger.new('/root/fax.log', 'monthly')

config_file =  "/root/config.yaml"
unless File.exist?(config_file)
	puts "Missing configuration file"
	exit 1
end
env = YAML.load(File.read(config_file))
browser = Mechanize.new
stream = RubyFS::Stream.new '127.0.0.1', 8021, 'ClueCon', lambda { |e| 
if e.class.to_s == "RubyFS::Event"
	if e.content[:event_name] == "CHANNEL_HANGUP_COMPLETE" && !e.content[:variable_fax_success].nil? &&  !e.content[:variable_custom_id].nil?
	         file_image_resolution=     e.content[:variable_fax_file_image_resolution] || "Not Applicable"
                image_pixel_size=  e.content[:variable_fax_image_pixel_size] || "Not Applicable"
                file_image_pixel_size=     e.content[:variable_fax_file_image_pixel_size] || "Not Applicable"
                longest_bad_row_run=       e.content[:variable_fax_longest_bad_row_run] || "Not Applicable"
                encoding=  e.content[:variable_fax_encoding] || "Not Applicable"
                encoding_name=     e.content[:variable_fax_encoding_name] || "Not Applicable"
                success=   e.content[:variable_fax_success] || "Not Applicable"
                result_code=       e.content[:variable_fax_result_code] || "Not Applicable"
                result_text=       e.content[:variable_fax_result_text] || "Not Applicable"
                ecm_used=  e.content[:variable_fax_ecm_used] || "Not Applicable"
                local_station_id=  e.content[:variable_fax_local_station_id] || "Not Applicable"
                remote_station_id= e.content[:variable_fax_remote_station_id] || "Not Applicable"
                document_transferred_pages=        e.content[:variable_fax_document_transferred_pages] || "Not Applicable"
                document_total_pages=      e.content[:variable_fax_document_total_pages] || "Not Applicable"
                image_resolution=  e.content[:variable_fax_image_resolution] || "Not Applicable"
                image_size=        e.content[:variable_fax_image_size] || "Not Applicable"
                bad_rows=  e.content[:variable_fax_bad_rows] || "Not Applicable"
                transfer_rate=     e.content[:variable_fax_transfer_rate] || "Not Applicable"
                fax_id = e.content[:variable_custom_id] || "Not Applicable"


		 logger.info("ASYNC RESPONSE #{fax_id} \n" +
                "fax_id=>" +  fax_id+" "+
                "fax_tiff_url=>" +  "#{env['FAX_REPO_URL']}/#{fax_id.strip}.tiff"+" "+
                "success=>" +        success+" "+
                "result_text=>" +   result_text+" "+
                "document_transferred_pages=>" +     document_transferred_pages+" "+
                "document_total_pages=>" +   document_total_pages+" "+
                "image_resolution=>" +       image_resolution+" "+
                "image_size=>" +     image_size+" "+
                "file_image_resolution=>" +       image_resolution+" "+
                "image_pixel_size=>" +    image_pixel_size+" "+
                "file_image_pixel_size=>" +      file_image_pixel_size +" "+
                "encoding=>" +    encoding+" "+
                "encoding_name=>" +       encoding_name+" "+
                "local_station_id=>" +    local_station_id+" "+
                "remote_station_id=>" +  remote_station_id +" "+
                "transfer_rate=>" +       transfer_rate)

		begin
	
		browser.post( "http://fax.smscountry.com/acknowledge" , {
		"fax_id" => fax_id,
		"fax_tiff_url" => "#{env['FAX_REPO_URL']}/#{fax_id.strip}.tiff",
		"success" =>       success,
		"result_text" =>  result_text,
		"document_transferred_pages" =>    document_transferred_pages,
		"document_total_pages" =>  document_total_pages,
		"image_resolution" =>      image_resolution,
		"image_size" =>    image_size,
		"file_image_resolution" =>      image_resolution,
		"image_pixel_size" =>   image_pixel_size,
		"file_image_pixel_size" =>     file_image_pixel_size ,
		"encoding" =>   encoding,
		"encoding_name" =>      encoding_name,
		"local_station_id" =>   local_station_id,
		"remote_station_id" => remote_station_id ,
		"transfer_rate" =>      transfer_rate
                })
		
		rescue Exception=> e
			logger.error "FAILED ASYNC RESPONSE for #{fax_id}  due to Exception --> #{e}"
		end
	
	end
end
}
stream.async.run

redis_sub = Redis.new
redis_sub.subscribe "ruby_channel" do |on|
	on.message do |channel,msg|
		data = JSON.parse msg
response = stream.api "originate {origination_caller_id_number=#{data['from']},ignore_early_media=true,absolute_codec_string='PCMU,PCMA',fax_enable_t38=true,fax_verbose=true,fax_use_ecm=true,fax_enable_t38_request=true,custom_id=#{data["fax_id"]}}sofia/gateway/trunk/#{data['fax_number']}   &txfax('/root/fax_repo/#{data["fax_id"]}.tiff')" 
response_status = response.content.split(" ")[0].strip
response_data = response.content.split(" ")[1].strip
fs_id = response.content.split(" ")[0]
logger.info "NEW FAX REQUEST fax_id #{data['fax_id']} fs_id #{fs_id} "

if(response_status[0]!="+")


 browser.post( "http://fax.smscountry.com/acknowledge" , {
                "fax_id" => data['fax_id'],
                "fax_tiff_url" => "#{env['FAX_REPO_URL']}/#{data['fax_id'].strip}.tiff",
                "success" =>     "0"  ,
                "result_text" =>  "Failed at first stage due to :#{response_data}",
                "document_transferred_pages" =>   "Not Applicable",
                "document_total_pages" =>   "Not Applicable",
                "image_resolution" =>       "Not Applicable",
                "image_size" =>     "Not Applicable",
                "file_image_resolution" =>       "Not Applicable",
                "image_pixel_size" =>    "Not Applicable",
                "file_image_pixel_size" =>     "Not Applicable",
                "encoding" =>    "Not Applicable",
                "encoding_name" =>       "Not Applicable",
                "local_station_id" =>    "Not Applicable",
                "remote_station_id" =>  "Not Applicable",
                "transfer_rate" =>      "Not Applicable"
                })

                logger.info("ASYNC RESPONSE #{data['fax_id']} \n" +
	         "fax_id=> " +  data['fax_id']+ "  " +
                "fax_tiff_url=> " +  "#{env['FAX_REPO_URL']}/#{data['fax_id'].strip}.tiff"+ "  " +
                "success=> " +      "0"  + "  " +
                "result_text=> " +   "Failed at first stage due to :#{response_data}"+ "  " +
                "document_transferred_pages=> " +    "Not Applicable"+ "  " +
                "document_total_pages=> " +    "Not Applicable"+ "  " +
                "image_resolution=> " +        "Not Applicable"+ "  " +
                "image_size=> " +      "Not Applicable"+ "  " +
                "file_image_resolution=> " +        "Not Applicable"+ "  " +
                "image_pixel_size=> " +     "Not Applicable"+ "  " +
                "file_image_pixel_size=> " +      "Not Applicable"+ "  " +
                "encoding=> " +     "Not Applicable"+ "  " +
                "encoding_name=> " +        "Not Applicable"+ "  " +
                "local_station_id=> " +     "Not Applicable"+ "  " +
                "remote_station_id=> " +   "Not Applicable"+ "  " +
                "transfer_rate=> " +       "Not Applicable"
                )

end




puts "originate reponse --------------------> #{response.inspect}"
	end
end
