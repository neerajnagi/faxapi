#rewring on place of erlang port of incoming 

#!/usr/bin/ruby

require_relative "./ESL"
require 'socket'
include Socket::Constants
require 'rubygems'
require 'mechanize'
require 'logger'
logger = Logger.new('/root/fax.log', 'monthly')

bind_address = "127.0.0.1"
bind_port = 8022

def time_now
  Time.now.strftime("%Y-%m-%d %H:%M:%S")
end

socket = Socket.new(AF_INET, SOCK_STREAM, 0)
sockaddr = Socket.sockaddr_in(bind_port, bind_address)
socket.bind(sockaddr)
socket.listen(5)
puts "Listening for connections on #{bind_address}:#{bind_port}"

config_file =  "/root/config.yaml"
unless File.exist?(config_file)
	puts "Missing configuration file"
        exit 
end
$env = YAML.load(File.read(config_file))
puts $env

loop do
  client_socket, client_sockaddr = socket.accept #_nonblock
  pid = fork do
    @con = ESL::ESLconnection.new(client_socket.fileno)
    info = @con.getInfo
    uuid = info.getHeader("UNIQUE-ID")
    @con.sendRecv("myevents")
    @con.sendRecv("divert_events on")
    @con.sendRecv("linger")
    puts "#{time_now} [#{uuid}] Call to [#{info.getHeader("Caller-Destination-Number")}]"
    @con.execute("log", "1, Wee-wa-wee-wa")
    @con.execute("info", "")
    @con.execute("answer", "")
    @con.execute("rxfax", "/root/fax_repo/#{uuid}.tif")
    
    while @con.connected
      e = @con.recvEvent

      if e
        name = e.getHeader("Event-Name")
        puts "EVENT: #{name}"
        break if name == "SERVER_DISCONNECTED"
        if name == "CHANNEL_HANGUP_COMPLETE"
         
		fax_success = e.getHeader("variable_fax_success")
                pages_transferred = e.getHeader("variable_fax_document_transferred_pages")
		pages_total = e.getHeader("variable_fax_document_total_pages")
		puts "#{fax_success}| #{pages_transferred} ----------------------- #{pages_total}"
                                if (fax_success.strip.to_i > 0) and( pages_transferred == pages_total)
                                        faxDestination = e.getHeader("Caller-Destination-Number")
                                        faxOrigin = e.getHeader("Caller-Orig-Caller-ID-Number")
                                        deliveryStartTime = e.getHeader( "Caller-Channel-Created-Time")
                                        deliveryEndTime =  e.getHeader("Caller-Channel-Hangup-Time")

					begin
				        browser = Mechanize.new
				        logger.info "INCOMING FAX  #{$env["INCOMING_FAX_NOTIFICATION_URL"]} -> from:#{faxOrigin}  to:#{faxDestination} delivery_start:#{deliveryStartTime} delivery_end:#{deliveryEndTime} attachment:#{$env['FAX_REPO_URL']}/#{uuid+'.tif'}"

				        page = browser.post( $env["INCOMING_FAX_NOTIFICATION_URL"] , {
               					 "from"=>faxOrigin,
        				        "to"=>faxDestination,
				                "delivery_start"=>(deliveryStartTime.to_i / 1000000).to_i,
				                "delivery_end"=>(deliveryEndTime.to_i / 1000000).to_i ,
				                "attachment"=>"#{$env['FAX_REPO_URL']}/#{uuid+'.tif'}" ,
				                "tatal_pages"=>pages_total
				                })

					rescue Exception => e
				        logger.error "Failed : INCOMING FAX NOTIFICATION to #{$env["INCOMING_FAX_NOTIFICATION_URL"]} -> from:#{faxOrigin}  to:#{faxDestination} delivery_start:#{deliveryStartTime} delivery_end:#{deliveryEndTime} attachment:#{$env['FAX_REPO_URL']}/#{uuid+'.tif'}"

					puts "exception raised #{e}	"
					end
					
				end
	#killing thread after channel hangup
	exit				

        end

      end
    end
  end

  Process.detach(pid)
end
