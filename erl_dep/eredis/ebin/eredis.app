{application,eredis,
             [{description,"Erlang Redis Client"},
              {vsn,"1.0.6"},
              {modules,[basho_bench_driver_eredis,basho_bench_driver_erldis,
                        eredis,eredis_client,eredis_parser,eredis_sub,
                        eredis_sub_client]},
              {registered,[]},
              {applications,[kernel,stdlib]}]}.
