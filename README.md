#Fax Api

##sync/async response details
###1) post api request received
###2) response will be
   status : failed  , reason : (due to some mandatory param missing or attachment related issue)
or
   status : dispatched , fax_id : ( fax id to be used in future correspondence)

###3) now this request could be immediately processed or processed later in case we have a task queue in place.
###4) processor will forward the request to freeswitch which will try to establish the channel.
###5) if there is some issue at this stage like , invalid number or B party not responding etc etc , an async response will be generate with fax_id from step (2).
###6) if channel is answered by B-party, fax transmission is initiated.
###7) at channel hangup async response is sent with either success or failure. Failure reason could be anything like failure in the middle of fax transfere, carrier lost in between etc.


##current openvz configuration of fax production server
```
openvz container finetuning
to check existing usage
cat /proc/user_beancounters
failcnt will give you some idea about which param needs to be fixed

current confiuration
kmemsize = 100mb (megabyte)
shmpages = 42kb (kilobit)
```
