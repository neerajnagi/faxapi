%% @author Mochi Media <dev@mochimedia.com>
%% @copyright faxapi Mochi Media <dev@mochimedia.com>

%% @doc Callbacks for the faxapi application.

-module(faxapi_app).
-author("Mochi Media <dev@mochimedia.com>").

-behaviour(application).
-export([start/2,stop/1]).


%% @spec start(_Type, _StartArgs) -> ServerRet
%% @doc application start callback for faxapi.
start(_Type, _StartArgs) ->
    faxapi_deps:ensure(),
    faxapi_sup:start_link().

%% @spec stop(_State) -> ServerRet
%% @doc application stop callback for faxapi.
stop(_State) ->
    ok.
