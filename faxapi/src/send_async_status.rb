require 'rubygems'
require 'mechanize'
require 'logger'  
logger = Logger.new('/root/fax.log', 'monthly')
begin
	config_file =  "/root/config.yaml"
        unless File.exist?(config_file)
                puts "Missing configuration file"
                exit 1
        end
        $env = YAML.load(File.read(config_file))


	browser = Mechanize.new
	logger.info "async fax notification to http://fax.smscountry.com/acknowledge ->#{ARGV[18]} variable_fax_success  =>  #{ARGV[6]},
variable_fax_result_text =>   #{ARGV[8]},
variable_fax_document_transferred_pages =>    #{ARGV[12]},
variable_fax_document_total_pages =>  #{ARGV[13]},
variable_fax_image_resolution =>      #{ARGV[14]},
variable_fax_image_size =>    #{ARGV[15]},
variable_fax_file_image_resolution => #{ARGV[0]},
variable_fax_image_pixel_size =>      #{ARGV[1]},
variable_fax_file_image_pixel_size => #{ARGV[2]},
variable_fax_longest_bad_row_run =>  #{ARGV[3]},
variable_fax_encoding =>      #{ARGV[4]},
variable_fax_encoding_name => #{ARGV[5]},
variable_fax_result_code =>  #{ARGV[7]},
variable_fax_ecm_used =>      #{ARGV[9]},
variable_fax_local_station_id =>      #{ARGV[10]},
variable_fax_remote_station_id =>     #{ARGV[11]},
variable_fax_bad_rows =>      #{ARGV[16]},
variable_fax_transfer_rate => #{ARGV[17]} "
  
page = browser.post( "http://fax.smscountry.com/acknowledge" , {
"fax_id" => ARGV[18],
"fax_tiff_url" => "#{$env['FAX_REPO_URL']}/#{ARGV[18].strip}.tiff",
"success" =>       ARGV[6],
"result_text" =>   ARGV[8],
"document_transferred_pages" =>    ARGV[12],
"document_total_pages" =>  ARGV[13],
"image_resolution" =>      ARGV[14],
"image_size" =>    ARGV[15],
"file_image_resolution" =>	ARGV[0],
"image_pixel_size" =>	ARGV[1],
"file_image_pixel_size" =>	ARGV[2],
#"variable_fax_longest_bad_row_run" =>	ARGV[3],
"encoding" =>	ARGV[4],
"encoding_name" =>	ARGV[5],
#"variable_fax_result_code" =>	ARGV[7],
#"ecm_used" =>	ARGV[9],
"local_station_id" =>	ARGV[10],
"remote_station_id" =>	ARGV[11],
#"bad_rows" =>	ARGV[16],
"transfer_rate" =>	ARGV[17]

		})

rescue Exception => e
	 logger.error "Failed #{e}| async fax notification #{ARGV[18]}"

end
