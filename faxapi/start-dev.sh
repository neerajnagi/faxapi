#!/bin/sh
# NOTE: mustache templates need \ because they are not awesome.
exec erl -pa ebin edit deps/*/ebin -boot start_sasl \
    -sname faxapi \
    -setcookie ClueCon \
    -s faxapi \
    -s reloader \
    -mnesia dir '"/root/faxapi/db/Mnesia.Smscountry"' \
    -detached
