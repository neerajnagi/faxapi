require 'rubygems'
require 'mechanize'
require 'logger'  
logger = Logger.new('/root/fax.log', 'monthly')
begin
	browser = Mechanize.new
	logger.info "incoming fax notification to #{ENV["INCOMING_FAX_NOTIFICATION_URL"]} -> from:#{ARGV[0]}  to:#{ARGV[1]} delivery_start:#{ARGV[2]} delivery_end:#{ARGV[3]} attachment:#{ENV['FAX_REPO_URL']}/#{ARGV[4]}"
	page = browser.post( ENV["INCOMING_FAX_NOTIFICATION_URL"] , {
		"from"=>ARGV[0], 
		"to"=>ARGV[1],
		"delivery_start"=>(ARGV[2].to_i / 1000000).to_i,
		"delivery_end"=>(ARGV[3].to_i / 1000000).to_i ,   
		"attachment"=>"#{ENV['FAX_REPO_URL']}/#{ARGV[4]}" , 
		"tatal_pages"=>ARGV[5] 
		})

rescue Exception => e
	logger.error "Failed : incoming fax notification to #{ENV["INCOMING_FAX_NOTIFICATION_URL"]} -> from:#{ARGV[0]}  to:#{ARGV[1]} delivery_start:#{ARGV[2]} delivery_end:#{ARGV[3]} attachment:#{ENV['FAX_REPO_URL']}/#{ARGV[4]}"

     puts "exception raised #{e}" 
end
