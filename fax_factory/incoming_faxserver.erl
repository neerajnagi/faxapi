-module(incoming_faxserver).
-import(freeswitch_msg, [get_var/2, get_var/3, sendmsg/3 ,sendmsg/2,sendmsg/1]).
-export([start/0,run/0,launch/1,loop/1,monitor_inbound_fax/1]).

start()->
  %% start our handler process running
  Pid = spawn(?MODULE,run,[]),
  %% register it with the same name as the module - myhandler
  register(?MODULE,Pid),
  mnesia:start().
  
run()->
  %% wait for messages from FreeSWITCH
  receive
    {call,Data}->
      %% a new call is starting, find the UUID
      %% _Rest is a list of all the channel variable in the form {"<name>","<value"}
      {event, [UUID | _Rest]} = Data,
      error_logger:info_msg("myhandler ~p: new call received, UUID is ~p~n~p ",[self(), UUID, _Rest]),
      run();
    {call_event,Data} ->
      %% we've got an event for a call we've been notified of already
      {event, [UUID | Rest]} = Data,
      %% find out the name of the event
      Name = proplists:get_value("Event-Name", Rest),
	error_logger:info_msg("myhandler ~p: UUID ~p, event ~p~n ",[self(), UUID,Name]),
	if Name == "CHANNEL_PARK" -> 
		error_logger:info_msg("answering the call",[])  ,
		Pid = spawn(?MODULE,monitor_inbound_fax , [UUID]);	
	true -> do_nothing 
	end,	
      run();
    {get_pid, UUID, Ref, Pid} ->
      %% request from FreeSWITCH for an outbound process to handle call at 'UUID'
      NewPid = spawn(?MODULE, run, []),
      error_logger:info_msg("myhandler ~p: request to spawn new handler process, returning ~p~n", [self(), NewPid]),
      Pid ! {Ref, NewPid},
      run()
  end.


monitor_inbound_fax(UUID) ->
%% rename tiff for outbound to sync with UUID
Freeswitch = list_to_atom(os:getenv("FS_NODE")),
{foo, Freeswitch } ! linger ,
 receive Linger_resp ->
  io:format("Linger response is ----------------> ~p ", [Linger_resp])
end,
{foo, Freeswitch } ! {sendmsg, UUID, [{"call-command", "execute"},{"execute-app-name","answer"  }  ]},
        receive
          _ ->
         error_logger:info_msg("||||||||||||||||||||||||||callback for ANSWER"),
%%      {foo, list_to_atom(os:getenv("FS_NODE"))} ! {sendmsg, UUID, [{"call-command", "execute"},{"execute-app-name","playback"  },{"execute-app-arg","silence_stream://2000"  }   ]},
%%              receive
%%                _ ->
%%              error_logger:info_msg("||||||||||||||||||||||||||callback for PLAYBACK"),
                {foo, list_to_atom(os:getenv("FS_NODE"))} ! {sendmsg, UUID, [{"call-command", "execute"},{"execute-app-name","rxfax"  },{"execute-app-arg","/root/fax_repo/"++UUID ++".tif"  }   ]},
%%                      receive
%%                       _ ->
%%                              error_logger:info_msg("||||||||||||||||||||||||||callback for rxfax"),
%%                              {foo, list_to_atom(os:getenv("FS_NODE"))} ! {sendmsg, UUID, [{"call-command", "execute"},{"execute-app-name","hangup"  }  ]} ,
                        receive
                        RXRESP -> error_logger:info_msg("||||||||||||||||||||||||||callback for rxfax with ~p",[RXRESP])
                        end
	end,


{foo,Freeswitch } ! {handlecall, UUID} , receive X____ ->
	do_nothing
end,
loop(UUID).



loop(UUID)->
receive Msg ->
        case Msg of
		 {call_event,Data} ->
		      {event, [UUID | Rest]} = Data,
		      EventName = proplists:get_value("Event-Name", Rest),
			io:format(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  ~p  <<<<<<<<<<<<<",[EventName]),
                        if EventName == "CHANNEL_HANGUP_COMPLETE" ->
				%% extracting metadata from Rest
		                {FaxSuccess,_} =string:to_integer( proplists:get_value("variable_fax_success" , Rest,"0")),
		                {PagesTransferred,_} = string:to_integer(proplists:get_value("variable_fax_document_transferred_pages", Rest,"0")),
				{PagesTotal,_} =string:to_integer( proplists:get_value("variable_fax_document_total_pages", Rest,"0")),
		                io:format("~p| ~p ----------------- ~p", [FaxSuccess , PagesTotal, PagesTransferred]),
                		if (FaxSuccess > 0) and( PagesTransferred == PagesTotal) ->
                       			 FaxDestination = proplists:get_value("Caller-Destination-Number" , Rest),
		                        FaxOrigin = proplists:get_value("Caller-Orig-Caller-ID-Number" , Rest),
                		        DeliveryStartTime = proplists:get_value( "Caller-Channel-Created-Time" , Rest),
		                        DeliveryEndTime =  proplists:get_value("Caller-Channel-Hangup-Time" , Rest),
                		        os:cmd("ruby  /root/fax_factory/send_fax.rb "++FaxOrigin++ "  " ++ FaxDestination   ++  "  " ++  DeliveryStartTime  ++  "  " ++ DeliveryEndTime  ++  "    " ++ UUID ++".tif " ++ integer_to_list(PagesTotal)  );

           		        true -> do_nothing
                        	end;
			true -> do_nothing
			end;
                _ -> do_nothing
        end,
Msg after 1200000 ->
exit("too old")
end,
loop(UUID).





launch(Ref) ->
  %% rpc call to a function to return a new outbound call pid
  NewPid = spawn(?MODULE, run, []),
  error_logger:info_msg("myhandler ~p: launch request, returning ~p~n", [self(), NewPid]),
  {Ref, NewPid}.
