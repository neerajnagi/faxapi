God.watch do |w|
        w.name  = "faxapi"
        w.interval = 2.second
        w.start  = "cd  /root/faxapi && ./start-dev.sh "
        w.log = "/var/log/god/faxapi.log"
        w.keepalive
        w.grace = 1.seconds
        w.start_if do |start|
                start.condition(:process_running) do |c|
                c.running = false
                end
         end
end

