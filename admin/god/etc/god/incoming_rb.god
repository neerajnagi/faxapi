God.watch do |w|
        w.name          = "incoming_rb"
        w.interval = 2.second
        w.start  = "cd  /root/fax_factory_esl && ruby  incoming_faxserver.rb "
        w.log = "/var/log/god/incoming_rb.log"
        w.keepalive
        w.grace = 1.seconds
        w.start_if do |start|
                start.condition(:process_running) do |c|
                c.running = false
                end
         end
end

